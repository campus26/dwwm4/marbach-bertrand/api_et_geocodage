import React, { useState } from 'react';
import MapComponent from './components/MapComponent';
import PoiInputComponent from './components/PoiInputComponent';
import './App.css';

function App() {
  const [pois, setPois] = useState([]);

  const handleAddPoi = (poi) => {
    setPois([...pois, poi]);
  };

  return (
    <>
      <div className="App">
        <MapComponent pois={pois} />
        <PoiInputComponent onAddPoi={handleAddPoi} />
      </div>
    </>
  );
}

export default App;
