// MapComponent.jsx
import React, { useRef, useEffect } from 'react';
import L from 'leaflet';
import 'leaflet/dist/leaflet.css';

const MapComponent = ({ pois }) => {
  const mapRef = useRef(null);

  useEffect(() => {
    console.log("in Map Component");

    // Check if the map container already exists in the ref
    if (!mapRef.current) {
      console.log("in test");

      // If the map container does not exist, create the map
      const map = L.map('map').setView([46.911, 6.32783], 13);

      L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png').addTo(map);

      // Display existing POIs on the map
      pois.forEach((poi) => {
        const marker = L.marker([poi.latitude, poi.longitude]).addTo(map);

        // Bind popup with the POI text
        marker.bindPopup(`Latitude: ${poi.latitude}<br>Longitude: ${poi.longitude}<br>Altitude: ${poi.altitude} feet`, {
          closeButton: false, // Disable the close button on the popup
        });

        // Toggle popup on marker click
        marker.on('click', () => {
          if (marker.isPopupOpen()) {
            marker.closePopup();
          } else {
            marker.openPopup();
          }
        });
      });

      // Set the map instance to the ref
      mapRef.current = map;
    } else {
      // If the map container exists, update the displayed POIs
      pois.forEach((poi) => {
        const marker = L.marker([poi.latitude, poi.longitude]).addTo(mapRef.current);

        // Bind popup with the POI text
        marker.bindPopup(`Latitude: ${poi.latitude}<br>Longitude: ${poi.longitude}<br>Altitude: ${poi.altitude} feet`, {
          closeButton: false, // Disable the close button on the popup
        });

        // Toggle popup on marker click
        marker.on('click', () => {
          if (marker.isPopupOpen()) {
            marker.closePopup();
          } else {
            marker.openPopup();
          }
        });
      });
    }
  }, [mapRef, pois]);

  console.log("before returning Map Component");
  return <div id="map" style={{ height: '500px' }}></div>;
};

export default MapComponent;
