// PoiInputComponent.jsx
import React, { useState } from 'react';

const PoiInputComponent = ({ onAddPoi }) => {
  const [latitude, setLatitude] = useState('');
  const [longitude, setLongitude] = useState('');
  const [altitude, setAltitude] = useState('');

  const handleAddPoi = () => {
    if (latitude && longitude && altitude) {
      const poi = {
        latitude: parseFloat(latitude),
        longitude: parseFloat(longitude),
        altitude: parseFloat(altitude),
      };
      onAddPoi(poi);

      // Clear input fields after adding POI
      setLatitude('');
      setLongitude('');
      setAltitude('');
    } else {
      alert("Attention au format des données pour insérer les coordonnées :\n==>Latitude (degrés décimaux D.dddddd, positif si nord, négatif si sud),\n==>Longitude (D.dddddd, degrés décimaux D.dddddd, positif si est, négatif si ouest),\n==>Altitude (pieds décimaux D.dddd).");
    }
  };

  return (
    <div>
      <h2>Add POI</h2>
      <div>
        <label>Latitude:</label>
        <input type="text" value={latitude} onChange={(e) => setLatitude(e.target.value)} />
      </div>
      <div>
        <label>Longitude:</label>
        <input type="text" value={longitude} onChange={(e) => setLongitude(e.target.value)} />
      </div>
      <div>
        <label>Altitude (feet):</label>
        <input type="text" value={altitude} onChange={(e) => setAltitude(e.target.value)} />
      </div>
      <button onClick={handleAddPoi}>Add POI</button>
    </div>
  );
};

export default PoiInputComponent;
